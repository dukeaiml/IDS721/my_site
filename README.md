# my_site


# Portfolio Website

This is a static site generated with Zola, showcasing my portfolio work for the IDS721 class.

## Live Site

[Live Portfolio Website](https://my-site-dukeaiml-ids721-55801ec9f9c516e047b9805edaad40ad56d8686.gitlab.io)

## Repository

[GitLab Repository](https://gitlab.com/dukeaiml/IDS721/my_site)

## Overview

The website includes a home page and portfolio project pages, each styled with CSS. The content highlights my work and achievements throughout the IDS721 course.

## Technologies Used

- Zola: Static site generator in Rust
- HTML, CSS: Front-end technologies

## Usage

To run this site locally:

1. Clone the repository: `git clone https://gitlab.com/dukeaiml/IDS721/my_site.git`
2. Install Zola: [Zola Installation Guide](https://www.getzola.org/documentation/getting-started/installation/)
3. Navigate to the project directory: `cd my_site`
4. Run the Zola build command: `zola build`
5. Serve the site locally: `zola serve`
6. Open your browser and visit `http://127.0.0.1:1111/` to view the site.

## Customization

Feel free to customize the content, styling, and structure based on your preferences and project requirements.

## License

This project is licensed under the [MIT License](LICENSE.md).

