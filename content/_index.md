+++
title = "Doyinsolami Olaoye"
description = "Welcome, this is Doyinsolami’s home on the web!"
+++

I am an engineer by training and I hope to use my research to design actionable solutions for society. I am particularly enthusiastic about initiatives that intertwine software engineering and machine learning. Projects involving the development of intelligent systems,scalable machine learning applications, or innovative algorithms that enhance user experiences greatly pique my interest. Throughout my academic and professional career, I've delved into diverse projects and roles that have honed my skills. 

I am currently a graduate student at Duke University studying Electrical and Computer Engineering specializing in Machine Learning and I am set to graduate in May, 2024. I am proficient in Python, C, and C++. In my pursuit of continuous learning and professional growth, I am currently exploring Rust, particularly drawn to its capabilities for performance-sensitive services. Additionally, I am currently preparing for both the AWS Certified Solutions Architect – Associate Certification and AWS Certified Machine Learning - Specialty Certification.