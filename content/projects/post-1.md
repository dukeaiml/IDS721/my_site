+++
title="Deep learning model to improve melanoma detection in people of color"
date=2022-05-31

[taxonomies]
categories = ["Publications"]
tags = ["Transfer Learning", "Machine Learning",]
+++


Melanoma is a type of skin cancer that is particularly dangerous to people with dark skin. This is due to the disease's late diagnosis and detection in people with dark skin. When melanoma is detected, the prognosis is often poor. Advancement in Artificial Intelligence (AI) technology and image classification has brought about tremendous progress and applications in medicine and diagnosis of skin cancer. Albeit, these techniques continue to produce unsatisfactory results when applied to people with dark skin. 

<!-- more -->
This work considered the trend in cancer detection using AI techniques. Dark skin melanoma clinical images were acquired and pre-processed to remove illumination and noise to aid the other stages such as segmentation and data augmentation. The acquired images were combined with a curated and augmented version of the Human Against Machines 10000 image (HAM10000) dataset and split into two classes: melanoma and non-melanoma. A pre-trained DenseNet121 was used as a base model for training and in addition, a transfer learning process was performed to exclude the top layer and fine-tune all the layers in the final Dense Block of the model pre-trained on ImageNet. The model achieved an accuracy of 99% for melanoma detection in white skin color and 98% for dark skin. The results show that the proposed model is effective in detecting melanoma in skin.

[Link to read published paper.](https://www.tandfonline.com/doi/pdf/10.1080/25765299.2023.2170066)