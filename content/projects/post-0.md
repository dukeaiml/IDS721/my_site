+++
title="Wildfire detection in Germany using AI"
date=2021-12-20

[taxonomies]
categories = ["Project Owner: Omdena + Dyrad"]
tags = ["Machine Learning"]
+++
<!-- https://www.linkedin.com/posts/doyinsolamiolaoye_wildfire-wildfire-climatechange-activity-6958198858984099840-2TlP?utm_source=share&utm_medium=member_desktop -->

The project's goal was to build an intelligent model that will detect fire of different types of wood through analysis of the existing sensor data, thereby enabling alarms for firefighters early enough, so they can extinguish it. During the period of eight (8) weeks, the team designed and implemented several data-based pipelines, leveraging the dataset provided by the Dryad team. I led the Knowledge Team responsible for for creating a knowledge library with valuable resources by identifying knowledge resources needed by other tasks teams to achieve their project goals.

<!-- more -->

The results of this project lie on the state of art machine learning models and correctly classify the sensor data into two categories, "in-smoke" and "clean-air".  The model developed in this project is scalable and replicable. Such a solution has the potential to reduce forest fires, thereby enabling alarms for firefighters early enough, so they can extinguish them. This will ultimately help to achieve the sustainable development goals in the areas of life on land and climate action.

After various models were tested, the TabNet classifier was selected as the final model. The model attained 96.3% precision and 96% F1-score on the test set. The final TabNet classifier's size is 96 kilobytes, and it took 292 seconds on GPU to fit it to the training data.

#### Testimonial from Project Leader: [link](https://www.linkedin.com/feed/update/urn:li:activity:6957305005443796993?commentUrn=urn%3Ali%3Acomment%3A%28activity%3A6957305005443796993%2C6957733492508012545%29&dashCommentUrn=urn%3Ali%3Afsd_comment%3A%286957733492508012545%2Curn%3Ali%3Aactivity%3A6957305005443796993%29)


